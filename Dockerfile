FROM maven:3.3.3-jdk-8 AS build
COPY src /home/nada/reservation-servicee/src
COPY pom.xml /home/nada/reservation-servicee
RUN mvn -f /home/nada/reservation-servicee/pom.xml clean package

#
# Package stage
#
FROM openjdk:11-jre-slim
COPY --from=build /home/nada/reservation-servicee/target/demo-0.0.1-SNAPSHOT.jar /usr/local/lib/demo.jar
EXPOSE 8888
ENTRYPOINT ["java","-jar","/usr/local/lib/demo.jar"]
